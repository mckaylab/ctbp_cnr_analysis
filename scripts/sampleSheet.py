import csv
import os
import fnmatch
import itertools

## basic script to generate sampleSheet.tsv for use in R
## run in directory with BigWig, Peaks, Bam dirs -- output from SLN CnR Pipeline

pwd = os.path.abspath('.')

bam = f'{pwd}/Bam'
peaks = f'{pwd}/Peaks'
bw = f'{pwd}/BigWig'

bam_ext = 'trim_q5_dupsRemoved_sorted.bam'
peak_ext = 'trim_q5_dupsRemoved_allFrags_peaks.narrowPeak'
bw_ext = 'trim_q5_dupsRemoved_allFrags_rpgcNorm.bw'
bwZ_ext = 'trim_q5_dupsRemoved_allFrags_rpgcNorm_zNorm.bw'

dirs = [bam, bw, peaks]
genotypes = ['yw']
stages = ['3LW']
tissues = ['wing']
targets = ['aCtBP']
fractions = ['sup', 'pel']
reps = ['Rep1']
genomes = ['dm6']

columns = ['genotype','time','tissue','target','fraction','rep','bam','bigWig','bigWigZnorm','peakFile']

with open('sampleSheet.tsv', 'w') as sht:
	ssheet = csv.writer(sht, delimiter = '\t')
	ssheet.writerow(columns)

	for genotype, stage, tissue, target, fraction, rep, genome in itertools.product(genotypes, stages, tissues, targets, fractions, reps, genomes):
		bamF = (f"{bam}/{genotype}_{stage}_{tissue}_{target}_{fraction}-{rep}_{genome}_{bam_ext}")
		peakF = (f"{peaks}/{genotype}_{stage}_{tissue}_{target}_{fraction}-{rep}_{genome}_{peak_ext}")
		bwF = (f"{bw}/{genotype}_{stage}_{tissue}_{target}_{fraction}-{rep}_{genome}_{bw_ext}")
		bwZF = (f"{bw}/{genotype}_{stage}_{tissue}_{target}_{fraction}-{rep}_{genome}_{bwZ_ext}")

		ssheet.writerow([genotype,stage,tissue,target,fraction,rep,bamF,bwF,bwZF,peakF])

	

		

